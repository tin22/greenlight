package main

import (
    "net/http"

    "github.com/julienschmidt/httprouter"
)

func (app *application) routes() http.Handler {
    router := httprouter.New()

    router.NotFound = http.HandlerFunc(app.notFoundResponse)

    router.MethodNotAllowed = http.HandlerFunc(app.methodNotAllowedResponse)
    
    router.HandlerFunc(http.MethodGet, "/v1/healthcheck", app.healthcheckHanler)
    router.HandleFunc(http.MethodPost, "/v1/movies", app.createMovieHanler)
    router.HandleFunc(http.MethodGet, "/v1/movies/:id", app.showMovieHandler)

    return router
}
